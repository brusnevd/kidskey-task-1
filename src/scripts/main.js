//Aliases
let Application = PIXI.Application,
    Container = PIXI.Container,
    loader = PIXI.loader,
    resources = PIXI.loader.resources,
    TextureCache = PIXI.utils.TextureCache,
    Sprite = PIXI.Sprite,
    Rectangle = PIXI.Rectangle;

//Create a Pixi Application
let app = new Application({
    width: window.innerWidth / 1.3,
    height: window.innerHeight / 1.3,
    antialias: true,
    transparent: false,
    resolution: 1
});

document.body.appendChild(app.view);

loader
    .add("../build/img/cat.png")
    .load(setup);

let cat, state, t, figures = [],
    mainReact, mainTrian, mainCirc;

function setup() {
    app.renderer.backgroundColor = "0x0ADAFF";

    let line = new PIXI.Graphics();
    line.lineStyle(5, 0xFFFFFF, 1);
    line.moveTo(0, app.renderer.view.height - app.renderer.view.height / 4);
    line.lineTo(app.renderer.view.width, app.renderer.view.height - app.renderer.view.height / 4);
    app.stage.addChild(line);

    let rectangle = new PIXI.Graphics();
    rectangle.lineStyle(2, 0x000000, 1);
    rectangle.beginFill(0xffffff);
    rectangle.drawRect(0, 0, 200, 100);
    rectangle.endFill();
    app.stage.addChild(rectangle);
    rectangle.x = line.x + line.width / 4;
    rectangle.y = app.renderer.view.height - app.renderer.view.height / 5;

    mainReact = rectangle;

    let triangle = new PIXI.Graphics();
    triangle.lineStyle(2, 0x000000, 1);
    triangle.beginFill(0xffffff);
    triangle.drawPolygon([
        50, 0, //First point
        0, 100, // Second
        100, 100
    ]);
    triangle.endFill();
    triangle.x = line.x + line.width / 2;
    triangle.y = app.renderer.view.height - app.renderer.view.height / 5;
    app.stage.addChild(triangle);

    mainTrian = triangle;

    let circle = new PIXI.Graphics();
    circle.lineStyle(2, 0x000000, 1);
    circle.beginFill(0xffffff);
    circle.drawCircle(50, 50, 50);
    circle.endFill();
    circle.x = line.x + line.width - line.width / 3;
    circle.y = app.renderer.view.height - app.renderer.view.height / 5;
    app.stage.addChild(circle);

    mainCirc = circle;

    for (let i = 0; i < 4; i++) {
        let rectangle = new PIXI.Graphics();
        rectangle.lineStyle(1, 0x000000, 1);
        rectangle.beginFill(0xFFFB00);
        rectangle.drawRect(0, 0, 100, 70);
        rectangle.pivot.set(50, 35);
        rectangle.endFill();
        rectangle.x = randowX();
        rectangle.y = randowY();
        rectangle.rotation = getRandomInt(10) / 10;
        app.stage.addChild(rectangle);
        rectangle.name = "rectangle";

        rectangle.interactive = true;
        rectangle.buttonMode = true;
        rectangle
            .on("pointerdown", onDragStart)
            .on("pointerup", onDragEnd)
            .on("pointerupoutside", onDragEnd)
            .on("pointermove", onDragMove);

        let triangle = new PIXI.Graphics();
        triangle.lineStyle(1, 0x000000, 1);
        triangle.beginFill(0x66FF33);
        triangle.drawPolygon([32, 0,
            0, 64,
            64, 64
        ]);
        triangle.rotation = getRandomInt(10) / 10;
        triangle.pivot.set(32, 32);
        triangle.endFill();
        triangle.x = randowX();
        triangle.y = randowY();
        app.stage.addChild(triangle);
        triangle.name = "triangle";

        triangle.interactive = true;
        triangle.buttonMode = true;
        triangle
            .on("pointerdown", onDragStart)
            .on("pointerup", onDragEnd)
            .on("pointerupoutside", onDragEnd)
            .on("pointermove", onDragMove);

        let circle = new PIXI.Graphics();
        circle.lineStyle(1, 0x000000, 1);
        circle.beginFill(0x9966FF);
        circle.drawCircle(0, 0, 32);
        triangle.pivot.set(32, 32);
        circle.endFill();
        circle.x = randowX();
        circle.y = randowY();
        circle.circular = true;
        app.stage.addChild(circle);
        circle.name = "circle";

        circle.interactive = true;
        circle.buttonMode = true;
        circle
            .on("pointerdown", onDragStart)
            .on("pointerup", onDragEnd)
            .on("pointerupoutside", onDragEnd)
            .on("pointermove", onDragMove);
    }

    figures.push(rectangle, triangle, circle);
}

function onDragStart(event) {
    this.data = event.data;
    this.event = event;
    this.dragging = true;
}

function onDragEnd() {
    if (this.name === "rectangle") {
        if (hitTestRectangle(this.event.target, mainReact)) {
            this.destroy();
        }
    } else if (this.name === "triangle") {
        if (hitTestRectangle(this.event.target, mainTrian)) {
            this.destroy();
        }
    } else if (this.name === "circle") {
        if (hitTestRectangle(this.event.target, mainCirc)) {
            this.destroy();
        }
    }

    this.alpha = 1;
    this.dragging = false;
    this.data = null;
}

function onDragMove() {
    if (this.dragging) {
        const newPos = this.data.getLocalPosition(this.parent);
        this.x = newPos.x;
        this.y = newPos.y;
    }
}

function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
}

function randowX() {
    let workPlaceWidth = app.view.width;

    let sign = getRandomInt(2);
    if (sign === 0) {
        sign = -1;
    } else {
        sign = 1;
    }

    return workPlaceWidth / 2 + (sign * getRandomInt(10) * 50);
}

function randowY() {
    let sign = getRandomInt(2);
    if (sign === 0) {
        sign = -1;
    } else {
        sign = 1;
    }

    let workPlaceHeight = app.view.height - app.view.height / 4;
    let result = workPlaceHeight / 2 + sign * (getRandomInt(10) * 17);

    return result;
}